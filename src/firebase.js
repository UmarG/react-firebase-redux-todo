import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAvfoyl2UfFEjFHdU_Koi7Dj5ORoLyfM3g',
  authDomain: 'firedux-todo-4dcd3.firebaseapp.com',
  databaseURL: 'https://firedux-todo-4dcd3.firebaseio.com',
  projectId: 'firedux-todo-4dcd3',
  storageBucket: '',
  messagingSenderId: '357720036711'
};

firebase.initializeApp(config);
const databaseRef = firebase.database().ref();
export const todosRef = databaseRef.child('todos');
